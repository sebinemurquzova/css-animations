# How this task will be evaluated
## We expect your solution to the task to meet the following criteria:


- You have created 2 variants of animations for the desktop version and mobile version
- You have added 2 images to the project using the markup and reused them for different animations
- You have written new rules for the selector in your file with styles. These rules use the images in the media section in order to be correctly shown in the mobile version. At the same time, you could reuse the animations in different versions of the website